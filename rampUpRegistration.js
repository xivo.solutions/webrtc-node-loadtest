const { registerUser } = require('./lib/helpers/userRegister');
const { loadCsvToJson, calculatePauseTime, delay } = require('./lib/helpers/utils');

const pauseTime = calculatePauseTime();

const registerAllUsers = async (users) => {
    for (let i = 0; i < users.length; i++) {
        registerUser(users[i]);
        await delay(pauseTime);
    }
}

(async () => {
    const users = await loadCsvToJson();
    await registerAllUsers(users);
    console.log('Ramp up finished.');
    process.exit();
})();
