'use strict';

const fetch = require('node-fetch');
const JSON = require('JSON');
const W3CWebSocket = require('websocket').w3cwebsocket;

class CtiLink {

	constructor(host, port, username, password, debug, cti) {
		this.host = host;
		this.port = port;
		this.username = username;
		this.pwd = password;
		this.timeout = 15;
		this.cti = cti
		this.cti.debugMsg = debug;
	}

	async login() {
		const options = {
			headers: {
				'accept': 'application/json',
				'content-type': 'application/json;charset=UTF-8',
			},
			body: `{"login":"${this.username}","password":"${this.pwd}"}`,
			method: 'POST',
		};

		const url = 'https://' + this.host + ':' + this.port + '/xuc/api/2.0/auth/login';

		return await fetch(url, options)
			.then((res) => {
				if (res.status === 200) {
					return res.json();
				}
				else {
					res.text().then(text => { console.log(`FETCH_RESULT: ${text}`) });
					throw new Error("Received error: ", res.status);
				}
			})
			.then((json) => {
				this.token = json.token;
				console.log(`Got token for user ${this.username}`);
				return this.token;
			})
			.catch((error) => {
				console.error(`ERROR: ${error}`);
				debugger;
			});
	};

	async test() {
		return await new Promise((resolve) => { setTimeout(() => { resolve(this.username); }, 1000) });
	}

	getSipPeer() {
		this.cti.setHandler(this.cti.MessageType.LOGGEDON, (() => {
			console.log('User ' + this.username + ' logged on CTI');
		}).bind(this));

		return new Promise((resolve) => {
			this.cti.setHandler(this.cti.MessageType.LINECONFIG, ((cfg) => {
				resolve(cfg);
			}));
		});

	};

	initWS() {
		const wsurl = 'wss://' + this.host + ':' + this.port + "/xuc/api/2.0/cti?token=" + this.token;
		this.cti.WebSocket.init(wsurl, this.username, this.pwd, W3CWebSocket);
	};

}

module.exports = CtiLink;