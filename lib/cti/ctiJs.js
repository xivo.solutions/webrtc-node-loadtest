const vm = require("vm");
const fs = require('fs');

const ctiJs = function () {
    context = { console: console, setInterval: setInterval };
    pathShotgun = './lib/cti/shotgun.js';
    pathCti = './lib/cti/cti.js';
    const fileShotgun = fs.readFileSync(pathShotgun);
    const fileCti = fs.readFileSync(pathCti);
    vm.runInNewContext(fileShotgun, context);
    vm.runInContext(fileCti, context);
    return context;
}

module.exports = ctiJs;