'use strict';

const JsSIP = require('jssip');

class JsSipUA {

    constructor(peer, socket, userAgent) {
        this.peerName = peer.name;
        this.peerPwd = peer.password;
        this.peerAddr = peer.sipProxyName;
        this.displayName = "XiVO WebRTC Registration Load Test";
        this.socket = socket;
        this.userAgent = userAgent;
        this.peerContactUri = `sip:${peer.name}@${peer.sipProxyName};rtcweb-breaker=no;transport=wss`;
        this.expires = '60'
        this.ua = this.createSipUa();
    };

    createSipUa() {
        let ua = new JsSIP.UA(
            {
                uri: `sip:${this.peerName}@${this.peerAddr}`,
                authorization_user: this.peerName,
                password: this.peerPwd,
                display_name: this.displayName,
                sockets: [this.socket],
                register: true,
                user_agent: this.userAgent,
                contact_uri: this.peerContactUri,
                register_expires: this.expires,
                log: {
                    level: 'debug'
                }
            });
        return ua;
    }

    setRegisterHandlers() {
        this.ua.on('registered', (function (e) { console.log('Peer ' + this.peerName + ' registered'); }).bind(this));
        this.ua.on('unregistered', (function (e) { console.warn('Peer ' + this.peerName + 'unregistered'); }).bind(this));
        this.ua.on('registrationFailed', (function (e) { console.error('Registration failed for peer ' + this.peerName) }).bind(this));
    }

    start() {
        this.ua.start();
        // console.log(`this.socket.isConnected(): ${this.socket.isConnected()}`);
        console.log(`JsSIP user agent created for peer: ${this.peerName}`);
    }

    checkState(user) {
        !this.socket.isConnected() && console.log(`Socket for user ${user.username} is disconnected.`);
        !this.ua.isRegistered() && console.log(`UA for peer ${this.peerName} is unregistered`);
    }

}

module.exports = JsSipUA;
