'use strict';

const csv = require('csv-parser')
const fs = require('fs')

class UserLoader {

    streamCSV(file) {
        console.log('Loading users from file: ', file);
        return fs.createReadStream(file).pipe(csv());
    };
}

module.exports = UserLoader;