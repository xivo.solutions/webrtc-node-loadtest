const csv = require('csv-parser');
const fs = require('fs');

const CALLS_PER_MINUTE = process.env.CALLS_PER_MINUTE;
const USERS_CSV = process.env.USERS_CSV;

const loadCsvToJson = () => {
    console.log(`USERS_CSV: ${USERS_CSV}`);
    return new Promise((resolve, reject) => {
        let result = []
        fs.createReadStream(USERS_CSV).pipe(csv())
            .on('data', (data) => result.push(data))
            .on('end', () => {
                console.log(`Finished loading ${result.length} users.`);
                resolve(result);
            })
            .on('error', () => {
                console.log('Error while loading CSV.')
                reject();
            });
    });
}

const calculatePauseTime = () => {
    if (CALLS_PER_MINUTE < 2) {
        console.log('callsPerMinute should be 2 or higher.');
        return;
    }
    return (60000 / (CALLS_PER_MINUTE - 1));
}

const delay = async (time) => {
    return new Promise(resolve => setTimeout(resolve, time));
};

module.exports = {
    loadCsvToJson: loadCsvToJson,
    calculatePauseTime: calculatePauseTime,
    delay: delay,
}