'use strict';

const NodeWebSocket = require('jssip-node-websocket');
const UA = require('../ua/jssipua');
const CtiLink = require('../cti/ctilink');
const Cti = require('../cti/ctiJs');

const LOADTEST_HOST = process.env.LOADTEST_HOST;
const LOADTEST_PORT = parseInt(process.env.LOADTEST_PORT);

const CTI_DEBUG = false;

const SIP_USER_AGENT = 'XiVO XC WebRTC';
const CHECK_STATE_INTERVAL = 10000;


const registerUser = (user) => {
    let cti = new CtiLink(LOADTEST_HOST, LOADTEST_PORT, user.username, user.password, CTI_DEBUG, Cti().Cti);
    cti.login().then(token => {
        if (token) {
            cti.getSipPeer().then(sipPeer => {
                let sipSocket = new NodeWebSocket(`wss://${LOADTEST_HOST}:${LOADTEST_PORT}/wssip?token=${token}`);
                let sipUa = new UA(sipPeer, sipSocket, SIP_USER_AGENT);
                sipUa.setRegisterHandlers();
                sipUa.start();
                setInterval(() => sipUa.checkState(user), CHECK_STATE_INTERVAL);
            });
            cti.initWS();
        }
        else {
            console.error(`Get token failed for user ${user.username} with password ${user.password}`);
        }
    });
}

module.exports = {
    registerUser: registerUser
}