'use strict';

const NodeWebSocket = require('jssip-node-websocket');
const UA = require('./lib/ua/jssipua');
const CtiLink = require('./lib/cti/ctilink');
const UserLoader = require('./lib/helpers/userLoader');
const Cti = require('./lib/cti/ctiJs');

const LOADTEST_HOST = process.env.LOADTEST_HOST;
const LOADTEST_PORT = parseInt(process.env.LOADTEST_PORT);
const USERS_CSV = process.env.USERS_CSV;

const CTI_DEBUG = true;

const SIP_USER_AGENT = 'XiVO XC WebRTC';
const CHECK_STATE_INTERVAL = 10000;

const userStream = new UserLoader().streamCSV(USERS_CSV);

userStream.on('data', user => {
    console.log('Got user ', user);
    let cti = new CtiLink(LOADTEST_HOST, LOADTEST_PORT, user.username, user.password, CTI_DEBUG, Cti().Cti);
    cti.login().then(token => {
        if (token) {
            cti.getSipPeer().then(sipPeer => {
                let sipSocket = new NodeWebSocket('wss://' + LOADTEST_HOST + ':' + LOADTEST_PORT + "/wssip?token=" + token);
                let sipUa = new UA(sipPeer, sipSocket, SIP_USER_AGENT);
                sipUa.setRegisterHandlers();
                sipUa.start();
                setInterval(() => sipUa.checkState(user), CHECK_STATE_INTERVAL);
                setTimeout(() => sipUa.checkState(user), 5000);
            });
            cti.initWS();
            setInterval(() => console.log('wainting'), 1000000);
        }
        else {
            console.error('Get token failed for user ' + user.username + ' with password ' + user.password);
        }
    });
});

userStream.on('end', () => {
    console.log('Users loaded');
})

